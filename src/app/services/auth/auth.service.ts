import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {
  private readonly USER_STORAGE = 'USER';
  private readonly BACKEND_URL = 'https://cors-anywhere.herokuapp.com/https://roomie-server.herokuapp.com/api/user';

  user$: BehaviorSubject<User>;

  private headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.headers = (new HttpHeaders()).append('Content-Type', 'application/json');

    const temp = localStorage.getItem(this.USER_STORAGE);
    this.user$ = new BehaviorSubject(temp ? JSON.parse(temp) : null);
  }

  private next(user: User) {
    this.user$.next(user);
    localStorage.setItem(this.USER_STORAGE, JSON.stringify(user));
  }

  register(user: User) {
    this.httpClient.post<User>(this.BACKEND_URL + '/register', JSON.stringify(user), { headers: this.headers })
      .subscribe((tempUser: User) => {
        this.next(tempUser);
      }, err => console.error(err));
  }

  login(email: string, password: string) {
    this.httpClient.post<User>(this.BACKEND_URL + '/login', {
      'email': email,
      'password': password
    }, { headers: this.headers })
      .subscribe((user: User) => {
        this.next(user);
      }, err => console.error(err));
  }

  logout(userId: number) {
    this.httpClient.post(this.BACKEND_URL + '/logout', {
      userId: userId
    }, { headers: this.headers })
      .subscribe(result => {
        this.next(null);
      }, err => console.error(err));
    this.next(null);
  }

  getUserInfo(userId: number): Observable<User> {
    return this.httpClient.get<User>(this.BACKEND_URL + '/userinfo/' + userId, { headers: this.headers });
  }

}

export class User {
  id: number;
  userId: number;
  email: string;
  first: string;
  last: string;
  phone: string;
  faculty: string;
  password: string;
  picture: string;
}
