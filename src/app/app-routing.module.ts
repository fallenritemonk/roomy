import { AppComponent } from './app.component';
import { BookedComponent } from './booked/booked.component';
import { FreeRoomsComponent } from './free/free.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProfileComponent } from './profile/profile.component';
import { MyAreaComponent } from './my-area/my-area.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/free', pathMatch: 'full' },
  {
    path: '',
    component: ProfileComponent,
    outlet: 'rightrouter'
  },
  {
    path: 'free',
    component: FreeRoomsComponent
  },
  {
    path: 'booked',
    component: BookedComponent
  },
  {
    path: 'my-area',
    component: MyAreaComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class AppRoutingModule { }
