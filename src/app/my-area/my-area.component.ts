import { Component, OnInit } from '@angular/core';
import { BookingService, Booking } from '../services/booking/booking.service';

@Component({
  selector: 'app-my-area',
  templateUrl: './my-area.component.html',
  styleUrls: ['./my-area.component.scss']
})
export class MyAreaComponent implements OnInit {

  history: Booking[];
  upcoming: Booking[];

  constructor(private bookingService: BookingService) { }

  ngOnInit() {
    this.getMyBookings();
  }

  private getMyBookings() {
    this.bookingService.getMyBookings().subscribe((bookings: Booking[]) => {
      const now = new Date();
      this.history = bookings.filter((booking: Booking) => booking.toDate < now);
      this.upcoming = bookings.filter((booking: Booking) => booking.toDate >= now);
    });
  }

  plusMoreTooltip(booking: Booking): string {
    let temp = '';
    for (let i = 2; i < booking.participants.length; i++) {
      temp += booking.participants[i].first + ' ' + booking.participants[i].last + ', ';
    }
    temp = temp.slice(0, temp.length - 2);
    return temp;
  }

  cancel(bookingId: number) {
    this.bookingService.deleteBooking(bookingId)
      .then(() => this.getMyBookings());
  }

}
