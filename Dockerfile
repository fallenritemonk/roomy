FROM tomcat:8

# CATALINA_HOME = /usr/local/tomcat

# Remove docs, examples and ROOT projects from webapps folder
RUN rm -r ${CATALINA_HOME}/webapps/docs ${CATALINA_HOME}/webapps/examples ${CATALINA_HOME}/webapps/ROOT ${CATALINA_HOME}/webapps/manager

# Rewrite all urls to index
RUN sed -i -r 's/<\/Context>//' ${CATALINA_HOME}/conf/context.xml &&\
  echo '<!-- REWRITE VALVE -->' >> ${CATALINA_HOME}/conf/context.xml &&\
  echo '<Valve className="org.apache.catalina.valves.rewrite.RewriteValve" />' >> ${CATALINA_HOME}/conf/context.xml &&\
  echo '</Context>' >> ${CATALINA_HOME}/conf/context.xml
RUN mkdir ${CATALINA_HOME}/roomy &&\
  mkdir ${CATALINA_HOME}/roomy/WEB-INF
RUN echo 'RewriteCond %{REQUEST_URI} !^.*\.(bmp|css|gif|htc|html?|ico|jpe?g|js|pdf|png|swf|txt|xml|svg|eot|woff|woff2|ttf|map)$' > ${CATALINA_HOME}/roomy/WEB-INF/rewrite.config &&\
  echo 'RewriteRule ^(.*)$ /index.html [L]' >> ${CATALINA_HOME}/roomy/WEB-INF/rewrite.config

# Set webapps roomy folder as root
RUN sed -i '/<\/Host>/i <Context docBase="../roomy/" path="" \/>' ${CATALINA_HOME}/conf/server.xml

# Deploy client
ADD dist/ ${CATALINA_HOME}/roomy/
